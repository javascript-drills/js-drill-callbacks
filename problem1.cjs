// Problem 1:
    
// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Create a directory of random JSON files
//     2. Delete those files simultaneously 

const fs =  require('fs');

function createFiles( callback ){

    fs.mkdir("./jsonDirectory",{recursive:true}, (err) => {
        if(err){
            console.log(`Can not create directory as: ${err}`);
            return;
        }
        else{
            for(let index=0; index<5; index++){
                fs.writeFileSync(
                    `./jsonDirectory/file${index}.json`, 
                    `File ${index} created.`
                )
                console.log(`Created file ${index}`);
            }
        }
    
        callback();
    
    });
}

function deleteFiles(){
    for(let index=0; index<5; index++){
        fs.unlink(`./jsonDirectory/file${index}.json`, function(err){
            if(err){
                return err.message;
            }
            console.log(`Deleted File ${index}`);
        });
    }
}

module.exports = {createFiles, deleteFiles};