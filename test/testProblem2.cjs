const performOperations = require("../problem2.cjs")

try{
    performOperations();
}catch(err){
    console.log(`Can not call the function due to: ${err.name}`);
}