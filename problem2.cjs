// Problem 2:
    
// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Read the given file lipsum.txt
//     2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//     3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//     4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//     5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
const fs = require('fs');

function performOperations(){
    fs.readFile(
        "lipsum.txt", 'utf8', (err, data) => {
            if(err){
                console.log(`Can't read file due to: ${err.message}`);
                return;
            }
            else{
                console.log("1. lipsum.txt file read successfully.")
                ///////////////////////////////////////////////////////////////////////////////1
                fs.writeFile(
                    "./upperCaseFile.txt", data.toUpperCase(), (err) => {
                        if(err){
                            console.log(`Can't write uppercase due to: ${err.message}`);
                            return;
                        }
                        else{
                            console.log('2. Converted to upper case and upperCaseFile.txt written successfully');
                            fs.writeFile("filenames.txt", "upperCaseFile.txt\n", (err) => {
                                if(err){
                                    console.log(`Can't write file name to filenames.txt file due to: ${err.message}`);
                                    return;
                                }
                                else{
                                    console.log("upperCaseFile.txt file name added to filenames.txt file");
                                    /////////////////////////////////////////////////////////////////////////////////2
                                    fs.readFile("upperCaseFile.txt", 'utf8', (err, data) => {
                                        if(err){
                                            console.log(`Can't read upperCaseFile.txt file due to: ${err}`);
                                            return;
                                        }
                                        else{
                                            console.log("3. upperCaseFile.txt read successfully.")
                                            let lowerCaseData = data.toLowerCase();
                                            let paragraphSplitData = lowerCaseData.split("\n\n");
                                            let sentenceData = "";
    
                                            paragraphSplitData.forEach( (paragraphElement)=>{
                                                let sentence = paragraphElement.split(". ");
                                                sentence.forEach( sentenceValue => {
                                                    sentenceValue += ".";
                                                    sentenceData += (sentenceValue + "\n");
                                                })
                                            })
                                            console.log("upperCaseFile.txt converted to lowerCase and splitted successfully.");
    
                                            fs.writeFile("./lowerCaseFile.txt", sentenceData, (err) => {
                                                if(err){
                                                    console.log(`Can't write the new file due to: ${err.message}`);
                                                    return;
                                                }
                                                else{
                                                    console.log("lowerCaseFile.txt written successfully.");
                                                    fs.appendFile("filenames.txt", "lowerCaseFile.txt\n", (err) => {
                                                        if(err){
                                                            console.log(`Unable to add file name due to: ${err.message}`);
                                                            return;
                                                        }
                                                        else{
                                                            console.log("lowerCaseFile.txt file name added to filenames.txt");
                                                            ////////////////////////////////////////////////////////////////////3
                                                            fs.readFile("lowerCaseFile.txt", "utf8", (err, data) => {
                                                                if(err){
                                                                    console.log(`Unable to read lowerCaseFile.txt due to: ${err.message}`);
                                                                }
                                                                else{
                                                                    console.log("4. lowerCaseFile.txt read successfully.");
                                                                    
                                                                    let lowerCaseData = data.split("\n");
                                                                    let sortedData = lowerCaseData.sort();
                                                                    sortedData = JSON.stringify(sortedData);
                                                                
                                                                    fs.writeFile("./sortedFile.txt", sortedData, (err) => {
                                                                        if(err){
                                                                            console.log(`Can't write the new file due to: ${err.message}`);
                                                                            return;
                                                                        }
                                                                        else{
                                                                            console.log("sortedFile.txt written successfully.");
                                                                            fs.appendFile("filenames.txt", "sortedFile.txt", (err) => {
                                                                                if(err){
                                                                                    console.log(`Unable to add file name due to: ${err.message}`);
                                                                                    return;
                                                                                }
                                                                                else{
                                                                                    console.log("sortefFile.txt name added to filenames.txt");
                                                                                    ////////////////////////////////////////////////////////////////////4
                                                                                    fs.readFile("filenames.txt", "utf8", (err, data) => {
                                                                                        if(err){
                                                                                            console.log(`Unable to read file due to: ${err.message}`);
                                                                                            return;
                                                                                        }
                                                                                        else{
                                                                                            console.log("5. filenames.txt read successfully.")
                                                                                            let fileNameArray = data.split("\n")

                                                                                            for(let index=0; index<fileNameArray.length; index++){
                                                                                                fs.unlink(`./${fileNameArray[index]}`, (err) => {
                                                                                                    if(err){
                                                                                                        console.log(`Unable to delete file due to ${err.message}`);
                                                                                                        return;
                                                                                                    }
                                                                                                    else{
                                                                                                        console.log(`Deleted file ${fileNameArray[index]}`);
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    }
                )
            }
        }
    )
}

module.exports = performOperations;